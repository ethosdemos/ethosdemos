module.exports = {
  title: 'EthosDemos',
  description: 'The blog of the EthosDemos podcast',
  keywords: ['eleventy', 'template', 'simple', 'clean'],
  // your site url without trailing slash
  url: 'https://vredeburg.netlify.app',
  // how many posts you want to show for each page
  paginate: 6
  // if you want to add disqus to your site
  // disqusShortname: "your-shortname"
};
